---
title: エスコフィエ『料理の手引き』全注解
author:
- 五 島　学（責任編集・訳・注釈）
- 河井 健司（訳・注釈）
- 春野 裕征（訳）
- 山 本　学（訳）
- 高 橋　昇（訳）
pandoc-latex-environment:
    main: [main]
    recette: [recette]
    frsubenv: [frsubenv]
    frsecenv: [frsecenv]
    frsecbenv: [frsecbenv]
    frchapenv: [frchapenv]
    center:[center]
...


<div class="main">
### アローズ[^1] {#alose}

<div class="frsecbenv">Alose</div>


\index{あろーす@アローズ}
\index{にしんたまし@ニシンダマシ|see アローズ}
\index{alose@alose}


<div class="center">
（約10人分の材料は1.2 kg〜1.5 kg）
</div><!--endCenter--> 
</div><!--endMain-->

[^1]: アローサとも呼ばれるニシン科の回遊魚で鮭のように川を遡上する。和名ニシンダマシ。


<div class="recette">


#### アローズ・ファルシ {#alose-farcie}

<div class="frsubenv">Alose Farcie</div>
\index{あろーす@アローズ!ふあるし@---・ファルシ}
\index{ふあるし@ファルシ!あろーす@アローズ・---}
\index{alose@alose!farci@--- farcie}
\index{farci@farci(e)!alose@Alose ---}
<!--\srcBercy{alose farcie}{Alose Farcie}{あろーすふあるし}{アローズ▼なかファルシ}-->


アローズはわたを抜き、中に[魚のファルス A](#farce-poisson-a)を詰める。

</div><!--endRecette-->
