;;Helm

;(require 'helm-config)
;(global-set-key (kbd "C-;") 'helm-mini)
(global-set-key (kbd "C-x b") 'helm-mini)
(global-set-key (kbd "C-x C-f") 'helm-find-files)
(global-set-key (kbd "M-y") 'helm-show-kill-ring)
(global-set-key (kbd "C-c h o") 'helm-occur)
(global-set-key (kbd "C-x c") 'helm-find)
(global-set-key (kbd "M-x") 'helm-M-x)
